#/usr/bin/env python
import boto3, urllib, json
role_name=raw_input("Please enter the name of the role:")
policy_arn=raw_input("Please enter the policy ARN to attach to the above role[Format - arn:aws:iam::aws:policy/<policy name>, Eg : arn:aws:iam::aws:policy/Modify-bucket-test]:")

iam = boto3.client('iam')

basic_role_policy = {
  "Version": "2012-10-17",
  "Statement": {
    "Effect": "Allow",
    "Principal": {"Service": "s3.amazonaws.com"},
    "Action": "sts:AssumeRole"
  }

}

role = iam.create_role(
   Path='/outware_devops/', 
   RoleName=role_name,
   AssumeRolePolicyDocument=json.dumps(basic_role_policy),
   Description=' to update the bucket contents',
)

response = iam.attach_role_policy(
    PolicyArn=policy_arn,
    RoleName=role_name,
)

print(response)
