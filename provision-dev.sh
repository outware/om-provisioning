#!/usr/bin/env bash
# provisioning for dev on vagrant requirements only
# https://gist.github.com/DaRaFF/3995789

sudo apt-get -y install build-essential binutils-doc

mkdir -p /vagrant/log
sudo sed -i "s/display_errors\s*= Off/display_errors = On/g" /etc/php5/apache2/php.ini
sudo sed -i "s/error_reporting\s*=.*/error_reporting = E_ALL \& ~E_NOTICE \& ~E_STRICT \& ~E_DEPRECATED/" /etc/php5/apache2/php.ini
sudo sed -i "s/display_startup_errors\s*= Off/display_startup_errors = On/g" /etc/php5/apache2/php.ini
sudo sed -i 's/;error_log\s*=.*/error_log = \/var\/log\/php.log/' /etc/php5/apache2/php.ini /etc/php5/cli/php.ini
sudo touch /var/log/php.log
sudo chown www-data:www-data /var/log/php.log

sudo service apache2 restart

sudo sed -i "s/bind-address\s*=\s*127.0.0.1/bind-address = 0.0.0.0/" /etc/mysql/my.cnf
sudo sed -i "s/log_error\s*=.*/log_error = \/var\/log\/mysql-error.log/" /etc/mysql/my.cnf

# Add logging mysql
# http://www.pontikis.net/blog/how-and-when-to-enable-mysql-logs
sudo touch /var/log/mysql.log
sudo touch /var/log/mysql-error.log
sudo touch /var/log/mysql-slow.log
sudo chown mysql:mysql /var/log/mysql*.log

MYSQL_LOG=$(cat <<EOF
general_log_file        = /var/log/mysql.log
general_log             = 1
log_slow_queries       = /var/log/mysql-slow.log
long_query_time = 2
log-queries-not-using-indexes
EOF
)
echo "${MYSQL_LOG}"  | sudo tee -a /etc/mysql/my.cnf

sudo service mysql restart

# This is for vagrant setup
sudo ln -fs /vagrant /var/vhosts/${HOSTNAME}/current

# Create a dev user - dont use root for applications
# echo "GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' IDENTIFIED BY 'root' WITH GRANT OPTION" | mysql -u root -ppassword
# echo "GRANT PROXY ON ''@'' TO 'root'@'%' WITH GRANT OPTION" | mysql -u root -ppassword
