# IS Provisioning Scripts
- Standard Vagrantfile
- Various bash scripts for setting up vagrant or AWS servers
- Should build all the way to running service without any user interaction (except maybe a password)

# Creating a virtualised dev env for your middleware repo
- Copy [om-provisioning/Vagrantfile](https://bitbucket.org/outware/om-provisioning/raw/spencer/Vagrantfile) to your repo
- edit HOSTNAME
- uncomment specific Vagrantfile shell provisioning scripts for your dev env
- make a provision-project.sh (or comment it out of your Vagrantfile)
- run rake secret to generate SECRET_KEY_BASE

# SSL
In addition to the Vagrantfile, copy the `/ssl` folder and contents locally

# First run only

```
$ vagrant plugin install vagrant-hostmanager
$ vagrant plugin install vagrant-cachier
$ vagrant plugin install vagrant-vbguest
```

# Spin up your machine

```
$ vagrant up
```

# Database

```
# password=password
https://HOSTNAME.dev/adminer?username=root
```

# TODO
- v2 King
    + 16.04
    + .env and provision-project rather than multiple vagrant scripts
    + full remote provision and deployment
    + review switch to Ansible
    + review switch to Docker for less HDD usage when across many projects
    + base box publishing
    + CI for base box and AWS AMI
    +

