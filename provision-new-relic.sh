#!/usr/bin/env bash

# Newrelic Settings
NEWRELIC_PHP_APP_NAME="APP_NAME"
NEWRELIC_LICENSE_KEY=XXXXXXXX

sudo sh -c 'echo "deb http://apt.newrelic.com/debian/ newrelic non-free" > /etc/apt/sources.list.d/newrelic.list'
wget -O - https://download.newrelic.com/548C16BF.gpg | sudo apt-key add -
sudo apt-get update
sudo apt-get install -y newrelic-sysmond

sudo nrsysmond-config --set license_key=$NEWRELIC_LICENSE_KEY
sudo /etc/init.d/newrelic-sysmond start
