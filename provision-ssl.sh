#!/usr/bin/env bash

# Specify where we will install
# the apache certificate
SSL_DIR="/etc/apache2/ssl"

# A blank passphrase
PASSPHRASE=""

# Set our CSR variables
SUBJ="
C=AQ
ST=Antarctica
O=
localityName=South Pole
commonName=$HOSTNAME
organizationalUnitName=
emailAddress=
"

# Create our SSL directory
# in case it doesn't exist
sudo mkdir -p "$SSL_DIR"

# Generate our Private Key, CSR and Certificate
sudo openssl genrsa -out "$SSL_DIR/apache.key" 2048
sudo openssl req -new -subj "$(echo -n "$SUBJ" | tr "\n" "/")" -key "$SSL_DIR/apache.key" -out "$SSL_DIR/apache.csr" -passin pass:$PASSPHRASE
sudo openssl x509 -req -days 365 -in "$SSL_DIR/apache.csr" -signkey "$SSL_DIR/apache.key" -out "$SSL_DIR/apache.crt"

echo 'Enable ssl...'
sudo a2enmod ssl
curl https://bitbucket.org/outware/om-provisioning/raw/spencer/ssl/default-ssl.conf | envsubst | sudo tee /etc/apache2/sites-available/default-ssl.conf
sudo a2ensite default-ssl

# disable Port 80
PORTS="
<IfModule ssl_module>
  Listen 443
</IfModule>

<IfModule mod_gnutls.c>
  Listen 443
</IfModule>
"
echo "${PORTS}"  | sudo tee /etc/apache2/ports.conf

sudo rm -rf /etc/apache2/sites-enabled/000-default.conf

sudo service apache2 restart
