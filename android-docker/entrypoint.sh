#!/bin/bash

# See License file /docker/LICENSE.TXT for license details specifically for this script.

# Add local user
# Either use the LOCAL_USER_ID if passed in at runtime or
# fallback

USER_ID=${LOCAL_USER_ID:-9001}

echo "Starting with UID : $USER_ID"
useradd --shell /bin/bash --no-create-home -u $USER_ID -o -c "" user
export HOME=/home/user

gosu user "$@"
