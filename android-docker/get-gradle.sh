#!/bin/bash
# Script that loads and installs gradle of the specified version
# Installation directory is passed through as docker needs it too
# so keeping it in one place as a param.
# 
# Problem this is trying to solve is that linux repos don't have all gradle versions.
# 
# Run like this:
#    ./get-gradle.sh <DIR-TO-INSTALL-IN> <VERSION>
if [ -z "$1" ] || [ -z "$2" ]
then 
	echo "Version or install dir is not specified"
	exit 1
else 
	GRADLE_TMP_DIR=$1
	GRADLE_VERSION=$2
	GRADLE_INSTALL_DIR=$GRADLE_TMP_DIR/gradle-$GRADLE_VERSION
	GRADLE_BIN=$GRADLE_INSTALL_DIR/bin
	echo "Installing gradle-$GRADLE_VERSION"
	# check if curl is installed
	if ! foobar_loc="$(type -p "wget")" || [ -z "$foobar_loc" ]; then
  		echo "wget is not installed. Installing..."
  		sudo apt-get install wget
	fi
	if ! foobar_loc="$(type -p "$GRADLE_BIN/gradle")" || [ -z "$foobar_loc" ]; then
	  	echo "Fetching gradle-$GRADLE_VERSION"
	  	# commands below are not sudo because this script is used in docker
		wget "https://services.gradle.org/distributions/gradle-$GRADLE_VERSION-bin.zip"
		mkdir $GRADLE_TMP_DIR
		unzip -d $GRADLE_TMP_DIR "gradle-$GRADLE_VERSION-bin.zip"
	fi
	# just in case touching .profile - it may already have export line in it
	source "$HOME/.profile"
	if ! [[ ":$PATH:" == *":$GRADLE_BIN:"* ]]; then
		echo "Setting up the PATH"
		echo "export PATH=\$PATH:$GRADLE_BIN" >> "$HOME/.profile"
		# reload profile. This will only work for within the script and not open terminals
		source "$HOME/.profile"
		export PATH=$PATH:$GRADLE_BIN
	fi
fi

# docker needs ENV to be specified