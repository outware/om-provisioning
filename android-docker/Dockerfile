FROM openjdk:8

# --------------------------
# --- Install Git and dependencies
# All installs are run in one step to make the image smaller

# --------------------------
# --- Add apt repositories
# For PPAs
# --------------------------
# --- Base pre-installed tools
# Common, useful
# --- Install Gradle from PPA
# --- Cleanup

RUN \
  dpkg --add-architecture i386 \
  && apt-get update \
  && apt-get install -y file git curl \
  libncurses5:i386 libstdc++6:i386 zlib1g:i386 \
  gosu \
  && apt-get install -y software-properties-common \
  && apt update \
  && ls -lah /etc/apt/sources.list.d/ \
  && add-apt-repository -y ppa:cwchien/gradle \
  && apt-get -y install python \
  && apt-get -y install build-essential \
  && apt-get -y install zip unzip \
  && apt-get update \
  && apt-get -y --allow-unauthenticated install gradle \
  && gradle -v \
  && rm -rf /var/lib/apt/lists /var/cache/apt \
  && apt-get autoremove -y

# ------------------------------------------------------
# --- Copy omscripts
# This is Outware's internal scripts repo
# Before building this image you must have cloned omscripts to your local machine in the same folder as the Dockerfile

ENV OMSCRIPTS /omscripts
COPY omscripts/ "${OMSCRIPTS}"
RUN ls -la "${OMSCRIPTS}"

# ------------------------------------------------------
# --- Create working directories
RUN \
  mkdir /workspace \
  && export WORKSPACE="/workspace" \
  && mkdir /home/user
WORKDIR /home/user


# ------------------------------------------------------
# --- Set up environment variables
ENV ANDROID_HOME="/opt/android-sdk-linux" \
   SDK_BUILD_URL="https://dl.google.com/android/repository/build-tools_r26.0.2-linux.zip" \
   SDK_TOOLS_URL="https://dl.google.com/android/repository/tools_r25.2.4-linux.zip"

# ------------------------------------------------------
# --- Download Android SDK Build Tools
RUN mkdir "$ANDROID_HOME" .android

# ------------------------------------------------------
# --- Download Android SDK tools into $ANDROID_HOME, accept license, delete files

RUN \
  cd "$ANDROID_HOME" \
  && curl -o android-sdk-tools.zip $SDK_TOOLS_URL \
  && unzip android-sdk-tools.zip \
  && rm android-sdk-tools.zip \
  && yes | $ANDROID_HOME/tools/bin/sdkmanager "extras;m2repository;com;android;support;constraint;constraint-layout;1.0.2" \
  && rm -rf $ANDROID_HOME/tools \
  && chmod -R 777 "$ANDROID_HOME"

# ------------------------------------------------------
# --- Copy & accept android licenses
RUN \
 mkdir -p "$ANDROID_HOME/licenses" \
 && echo -e "\nd56f5187479451eabf01fb78af6dfcb131a6481e" > "$ANDROID_HOME/licenses/android-sdk-license" \
 && echo -e "\n84831b9409646a918e30573bab4c9c91346d8abd" > "$ANDROID_HOME/licenses/android-sdk-preview-license"


# ------------------------------------------------------
# --- Workdir and revision

RUN chmod -R 777 /home/user
ENV PATH="${ANDROID_HOME}/tools:${ANDROID_HOME}/platform-tools:${PATH}"
ENV WORKSPACE /workspace
WORKDIR $WORKSPACE

COPY entrypoint.sh /usr/local/bin/entrypoint.sh
ENTRYPOINT ["/usr/local/bin/entrypoint.sh"]
