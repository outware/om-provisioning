#!/usr/bin/env bash

sudo hostname ${HOSTNAME}
echo "${HOSTNAME}"  | sudo tee /etc/hostname

sudo usermod -a -G www-data `whoami`
# newgrp www-data

echo 'Set Mysql defaults...'
sudo debconf-set-selections <<< "mysql-server mysql-server/root_password password ${MYSQL_PASS}"
sudo debconf-set-selections <<< "mysql-server mysql-server/root_password_again password ${MYSQL_PASS}"
sudo debconf-set-selections <<< "postfix postfix/mailname string ${HOSTNAME}"
sudo debconf-set-selections <<< "postfix postfix/main_mailer_type string 'Internet Site'"
export DEBIAN_FRONTEND=noninteractive

echo 'Linux Dependencies...'
sudo apt-get update
sudo apt-get upgrade -y
sudo apt-get install -y       \
apache2                       \
apache2-threaded-dev          \
apache2-utils                 \
autoconf                      \
bison                         \
build-essential               \
curl                          \
git-core                      \
libapache2-mod-php5           \
libapache2-mod-proxy-html     \
libcurl4-openssl-dev          \
libffi-dev                    \
libgdbm-dev                   \
libgdbm3                      \
libmysqlclient-dev            \
libncurses5-dev               \
libreadline6-dev              \
libssl-dev                    \
libyaml-dev                   \
mysql-server-5.5              \
nodejs                        \
ntp                           \
php5-cli                      \
php5-curl                     \
php5-gd                       \
php5-intl                     \
php5-mcrypt                   \
php5-mysql                    \
php5-xsl                      \
postfix                       \
python-software-properties    \
python-pip                    \
vim                           \
zlib1g-dev

sudo apt-get autoremove -y

echo 'Set up NTP time client'
TIME_SERVERS=$(cat <<EOF
server 0.ubuntu.pool.ntp.org
server 1.ubuntu.pool.ntp.org
server 2.ubuntu.pool.ntp.org
server 3.ubuntu.pool.ntp.org
EOF
)
echo "${TIME_SERVERS}"  | sudo tee /etc/ntp.conf
sudo service ntp restart

echo 'Set sup unattended-upgrades file...'
# unattended version of sudo dpkg-reconfigure -plow unattended-upgrades
UPDATES=$(cat <<EOF
APT::Periodic::Update-Package-Lists "1";
APT::Periodic::Unattended-Upgrade "1";
EOF
)
echo "${UPDATES}"  | sudo tee /etc/apt/apt.conf.d/20auto-upgrades

echo 'Importing time zone information into mysql'
mysql_tzinfo_to_sql /usr/share/zoneinfo | mysql -uroot -p${MYSQL_PASS} mysql

echo "ServerName `hostname`"  | sudo tee /etc/apache2/httpd.conf

echo "Installing Pip..."
curl -O https://bootstrap.pypa.io/get-pip.py
sudo python get-pip.py
rm get-pip.py

echo 'Install Adminer'
sudo mkdir /usr/share/adminer
sudo wget "http://www.adminer.org/latest.php" -O /usr/share/adminer/latest.php
sudo ln -s /usr/share/adminer/latest.php /usr/share/adminer/adminer.php
echo "Alias /adminer /usr/share/adminer/adminer.php" | sudo tee /etc/apache2/conf-available/adminer.conf
sudo a2enconf adminer.conf

sudo rm -rf /var/vhosts/${HOSTNAME}
sudo mkdir -p /var/vhosts/${HOSTNAME}/shared
sudo chown -R www-data:www-data /var/vhosts/${HOSTNAME}
sudo chmod 775 -R /var/vhosts/${HOSTNAME}

echo 'Enable mod_rewrite...'
sudo a2enmod rewrite authz_groupfile
sudo service apache2 restart
